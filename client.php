<!DOCTYPE html>
<?php require_once("bd.php");
if ($_GET) {
    $id = $_GET['id'];
    $quant = $_GET['quant'];
    $quant_tot = $_GET['total'];
    $nouvelle_quant = $quant_tot - $quant;

    $conn = connexionBd();
  
    $sql = "update produit set quantite='$nouvelle_quant' where id='$id'";
    $conn->query($sql);

    $sql = "select conteneur_id,volume from produit where id='$id'";
    $result = $conn->query($sql);

    $row = $result->fetch_assoc();
    $id_conteneur = $row['conteneur_id'];
    $volume = $row['volume'];

    $sql = "select taille_utilise from conteneur where id='$id_conteneur'";
    $result = $conn->query($sql);

    $row = $result->fetch_assoc();
    $taille_nouvelle = $row['taille_utilise']+$quant*$volume;

    $sql = "update conteneur set taille_utilise='$taille_nouvelle' where id='$id_conteneur'";
    if (!$conn->query($sql)) {
        echo "Erreur : (" . $conn->errno . ") " . $conn->error;
    }

    deconnexionBd($conn);
  
    $ajout = 1;
  }
  else {
    $ajout = 0;
  }
?>
<html lang="fr">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap.min.css">
    <script src="jquery-3.1.1.slim.min.js"></script>
  </head>
  <body>
  <?php include 'header.php'; ?>
  <div class="container">
  <h1>Acheter des produits</h1>
  <table class="table">
    <thead class="thead-default">
        <tr>
        <th>Produit</th>
        <th>Nom vendeur</th>
        <th>Quantité restante</th>
        <th>Prix</th>
        <th>Remplissage</th>
        <th>Quantité</th>
        <th>Acheter</th>
        </tr>
    </thead>
    <tbody>
    <?php 
       
    // afficher les conteneurs disponibles avec leur prix et un bouton "réserver"
    $conn = connexionBd();
    $sql = "SELECT * FROM produit";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            ?>
            <tr>   
            <td><?php echo $row["nom"]?></td>
            <td><?php echo $row["vendeur"]?></td>
            <td><?php echo $row["quantite"]?></td>
            <td><?php echo $row["prix"]?></td>
            <?php $conteneur_id = $row["conteneur_id"]; 
            $sql = "SELECT taille,taille_utilise FROM conteneur WHERE id='$conteneur_id'";
            $resultat = $conn->query($sql);
            $ligne = $resultat->fetch_assoc();
            $progress = $ligne["taille_utilise"]*100/$ligne["taille"];
            ?>
            <td><div class="progress"><div class="progress-bar" style="<?php echo "width:$progress"."%"?>"></div></div></td>
            <td><input type="number" id="<?php echo 'quant'.$row["id"] ?>"/></td>   
            <td><a id="<?php echo 'lien'.$row["id"] ?>" href="#" class="btn btn-primary" style="color:white">Acheter</a></td>
            
            <script>
            $("#<?php echo 'lien'.$row["id"] ?>").click(function() {
                var quantite = document.getElementById("<?php echo 'quant'.$row['id'] ?>").value;
                document.location.href = "client.php?id=<?php echo $row["id"];?>&quant="+quantite+"&total=<?php echo $row["quantite"]?>";
            });
            </script>
            </tr>
            <?php
        }
    } else {
        ?><td> 0 résultats</td><?php
    }
    $conn->close();
    
    ?>
    </tbody>
    </table>

    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="tether.min.js"></script>
    <script src="bootstrap.min.js"></script>
    </div>
  </body>
</html>
