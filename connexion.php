<!DOCTYPE html>
<html lang="fr">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="bootstrap.min.css">
  <link rel="stylesheet" href="style.css">
</head>
<body>
  <?php include 'header.php'  ?>
  <div class="container">
    <div class="card-deck">
      <div class="card">
        <img class="card-img-top card-image"  src="img/transport.jpg" alt="Card image cap">
        <div class="card-block">
          <h4 class="card-title">Transporteur</h4>
          <p class="card-text">Vous proposez des conteneurs à faire remplir par les fournisseurs.</p>
        </div>
        <div class="card-footer">
          <a href="transport.php" class="btn btn-success btn-lg btn-block">Je suis transporteur !</a>
        </div>
      </div>
      <div class="card">
        <img class="card-img-top card-image" src="img/fournisseur.jpg" alt="Card image cap">
        <div class="card-block">
          <h4 class="card-title">Fournisseur</h4>
          <p class="card-text">Vous proposez des produits pour remplir les conteneurs.</p>
        </div>
        <div class="card-footer">
          <a href="fournisseur.php" class="btn btn-success btn-lg btn-block">Je suis fournisseur !</a>
        </div>
      </div>
      <div class="card">
        <img class="card-img-top card-image" src="img/client.png"  alt="Card image cap">
        <div class="card-block">
          <h4 class="card-title">Client</h4>
          <p class="card-text">Je veux acheter des produits livré par conteneurs à bas prix.</p>
        </div>
        <div class="card-footer">
          <a href="client.php" class="btn btn-success btn-lg btn-block">Je suis client !</a>
        </div>
      </div>
    </div>

  </div>


  <!-- jQuery first, then Tether, then Bootstrap JS. -->
  <script src="jquery-3.1.1.slim.min.js"></script>
  <script src="tether.min.js"></script>
  <script src="bootstrap.min.js"></script>
</body>
</html>
