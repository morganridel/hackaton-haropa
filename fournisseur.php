<!DOCTYPE html>
<?php require_once("bd.php");
if ($_POST) {
  $nom = $_POST['nom'];
  $nom_produit = $_POST['nom_produit'];
  $volume = $_POST['volume'];
  $prix = $_POST['prix'];
  $quantite = $_POST['quantite'];
  $id = $_POST['id'];

  $sql = "INSERT INTO produit (nom, volume, prix, vendeur, conteneur_id, lieu_livraison, quantite) VALUES ('$nom_produit','$volume','$prix', '$nom', '$id', 'NULL', '$quantite')";
  $conn = connexionBd();

  if (!$conn->query($sql)) {
    echo "Erreur : (" . $conn->errno . ") " . $conn->error;
  }

  deconnexionBd($conn);

  $ajout = 1;
}
else {
  $ajout = 0;
}
?>
<html lang="fr">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap.min.css">
  </head>
  <body>
  <?php include 'header.php'; ?>
  <div class="container">
  <?php
    if ($ajout == 1) {
      echo '<div class="alert alert-success" role="alert">
      <strong>Succès !</strong> Les produits ont bien été ajoutés !
      </div>';
    } ?>
  <h1>Réserver un transporteur</h1>
  <table class="table">
    <thead class="thead-default">
        <tr>
        <th>Nom transporteur</th>
        <th>Date départ</th>
        <th>Date arrivée</th>
        <th>Volume conteneur</th>
        <th>Prix</th>
        <th>Réservation</th>
        </tr>
    </thead>
    <tbody>
    <?php 
       
    // afficher les conteneurs disponibles avec leur prix et un bouton "réserver"
    $conn = connexionBd();
    $sql = "SELECT * FROM conteneur";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            ?>
            <tr>   
            <td><?php echo $row["nom_transporteur"]?></td>
            <td><?php echo $row["date_depart"]?></td>
            <td><?php echo $row["date_arrivee"]?></td>
            <td><?php echo $row["taille"]." m&sup3;"?></td>
            <td><?php echo $row["fdp"]." €"?></td>
            <td><a href="produits.php?id=<?php echo $row["id"]?>" class="btn btn-primary" style="color:white">Réserver</a></td>
            </tr>
            <?php
        }
    } else {
        ?><td> 0 résultats</td><?php
    }
    $conn->close();
    
    ?>
    </tbody>
    </table>

    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="jquery-3.1.1.slim.min.js"></script>
    <script src="tether.min.js"></script>
    <script src="bootstrap.min.js"></script>
    </div>
  </body>
</html>
