<!DOCTYPE html>
<?php require_once("bd.php") ?>
<html lang="fr">
  <head>
    <title>Conteneur en Seine</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap.min.css">
  </head>
  <body>
    <?php include 'header.php'  ?>
    <!-- Font awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <style type="text/css">
  body {
    position: relative;
  }
  #main-jumbotron {
    background-image: url(ls.jpg);
    //background-size: cover;
  }

  .card-img-top {
    overflow: hidden;
  }

  #bottom-jumbotron {
    background-color: cyan;
  }
  </style>
  <title>App landing page</title>
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="50" >
  <!-- Scrollspy -->


  <div id="main-jumbotron"  style="background-image:url(rouen.jpg) !important" class="jumbotron text-center">
    <div class="text-center" style="width:60%;margin: 0 auto; padding: 15px 0 15px 0; background: rgba(255, 255, 255, 0.4)">
      <h1 class="display-3">Conteneurs en Seine</h1>

      <p class="lead font-weight-bold">Pour une vie plus Seine !</p>
      <hr class="my-4">
      <p>Envie de faire partie de notre communauté ? Abonnez-vous à notre newsletter !</p>
      <form action="" class="form-inline justify-content-center">
        <div class="input-group mr-2">
          <span class="input-group-addon" id="addon1">@</span>
          <input type="email" class="form-control" placeholder="Entrez votre email">
        </div>
        <a href="#" class="btn btn-primary">Envoyer</a>
      </form>
      </div>
  </div>

  <div id="about" class="container my-5">
  <h2 class="text-center">Un sytème économique responsable et innovant</h2>
  <p class="text-center">L'application du moment permettant de rapprocher les acheteurs, vendeurs et transporteurs!</p>
  </div>


  <div class="container mb-4">
    <div class="card-deck">

      <div class="card">
        <img src="ah.jpg" alt="" class="card-img-top" width="100%">
        <div class="card-block">
          <h4 class="card-title"><i class="fa fa-thumbs-up"></i> Une nouvelle façon de penser !</h4>
          <p class="card-text">Des offres défiantes toutes concurences disponible à portée de doigt! Et des fournisseurs plus proche de leurs clients.</p>
        </div>
      </div>

      <div class="card">
        <img src="flambeau.png" alt="" class="card-img-top" width="100%">
        <div class="card-block">
          <h4 class="card-title"><i class="fa fa-handshake-o"></i> Et cette sentence est irrévocable</h4>
          <p class="card-text">L'essayer, c'est l'adopter. Avec cette méthode vous pourrez effectuer des économies et participer de manière active à la qualité de l'air de votre  ville  !</p>
        </div>
      </div>

      <div class="card">
        <img src="immu.jpg" alt="" class="card-img-top" width="100%">
        <div class="card-block">
          <h4 class="card-title"><i class="fa fa-euro"></i> Réduire l'encombrement des villes</h4>
          <p class="card-text">Avec cette méthode de commercialisation, nous diminuons le nombre de camions ou de camionnettes effectuant des livraisons au sein d'une ville. Nous pouvons ainsi diminuer l'empreinte carbone de notre façon de commercer.  </p>
        </div>
      </div>
    </div>
  </div>

  <div id="bottom-jumbotron" class="jumbotron text-center mb-0" style="background: gray !important">
    <h4>Commencez dès maintenant à être acteur de votre environnement !</h4>
    <p>Qu'est ce qui vous retient ?!</p>
    <a href="#"><img src="app.jpg" alt="" width="200px"></a>

  </div>


    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="jquery-3.1.1.slim.min.js"></script>
    <script src="tether.min.js"></script>
    <script src="bootstrap.min.js"></script>
  </body>
</html>
