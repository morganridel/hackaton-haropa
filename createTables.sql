CREATE TABLE conteneur (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nom_transporteur VARCHAR(100),
    date_depart VARCHAR(100),
    date_arrivee VARCHAR(100),
    lieu_depart VARCHAR(100),
    lieu_arrivee VARCHAR(100),
    fdp INT,
    taille INT,
    taille_utilise INT
);

CREATE TABLE produit (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(100),
    volume INT,
    prix INT,
    vendeur VARCHAR(100),
    conteneur_id INT,
    lieu_livraison VARCHAR(100),
    quantite INT,
    FOREIGN KEY (conteneur_id) REFERENCES conteneur(id)
);
