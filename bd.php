<?php
function connexionBd() {
  $servername = "localhost";
  $username = "root";
  $password = "root";
  $database = "haropa";

  // Create connection
  $conn = new mysqli($servername, $username, $password, $database);

  // Check connection
  if ($conn->connect_error) {
     die("Connection failed: " . $conn->connect_error);
  }
  // echo "Connected successfully";

  return $conn;
}

function deconnexionBd($conn) {
  $conn->close();
}

?>
