<!DOCTYPE html>
<?php require_once("bd.php");
if ($_POST) {
  $nom = $_POST['nom'];
  $date_dep = $_POST['date-dep'];
  $date_arr = $_POST['date-arr'];
  $lieu_dep = $_POST['lieu-dep'];
  $lieu_arr = $_POST['lieu-arr'];
  $prix = $_POST['prix'];
  $taille = $_POST['taille'];

  $sql = "INSERT INTO conteneur (nom_transporteur,date_depart,date_arrivee,lieu_depart,lieu_arrivee,fdp,taille) VALUES ('$nom','$date_dep','$date_arr','$lieu_dep', '$lieu_arr','$prix','$taille')";
  $conn = connexionBd();

  if (!$conn->query($sql)) {
    echo "Erreur : (" . $conn->errno . ") " . $conn->error;
  }

  deconnexionBd($conn);

  $ajout = 1;
}
else {
  $ajout = 0;
}
?>
<html lang="fr">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="bootstrap.min.css">
</head>
<body>
  <?php include 'header.php'  ?>
  <div class="container">
    <?php
    if ($ajout == 1) {
      echo '<div class="alert alert-success" role="alert">
      <strong>Succès !</strong> Votre conteneur à bien été ajouté !
      </div>';
    } ?>
    <h1>Ajouter un conteneur</h1>
    <form method="post">
      <div class="form-group">
        <label for="nom">Nom transporteur : </label>
        <input type="text" class="form-control" id="nom" name="nom" placeholder="Entrez le nom du transporteur">
      </div>

      <div class="form-group">
        <label for="date-dep">Date de départ : </label>
        <input type="text" class="form-control" id="date-dep" name="date-dep" placeholder="Date de départ du conteneur">
      </div>

      <div class="form-group">
        <label for="date-arr">Date d'arrivée : </label>
        <input type="text" class="form-control" id="date-arr" name="date-arr" placeholder="Date d'arrivée du conteneur">
      </div>

      <div class="form-group">
        <label for="lieu-dep">Lieu du départ : </label>
        <input type="text" class="form-control" id="lieu-dep" name="lieu-dep" placeholder="Lieu du départ" >
      </div>

      <div class="form-group">
        <label for="lieu-arr">Lieu d'arrivée : </label>
        <input type="text" class="form-control" id="lieu-arr" name="lieu-arr" placeholder="Lieu d'arrivée" >
      </div>

      <div class="form-group">
        <label for="prix">Prix de transport : </label>
        <input type="number" class="form-control" id="prix" name="prix" >
      </div>

      <div class="form-group">
        <label for="taille">Volume (m&sup3;) : </label>
        <input type="number" class="form-control" id="taille" name="taille" >
      </div>

      <button type="submit" class="btn btn-primary">Valider</button>
    </form>
  </div>

  <!-- jQuery first, then Tether, then Bootstrap JS. -->
  <script src="jquery-3.1.1.slim.min.js"></script>
  <script src="tether.min.js"></script>
  <script src="bootstrap.min.js"></script>
</body>
</html>
