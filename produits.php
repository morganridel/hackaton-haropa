<!DOCTYPE html>
<?php require_once("bd.php");
?>
<html lang="fr">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap.min.css">
  </head>
  <body>
    <?php include 'header.php' ?>
    <?php $conn = connexionBd(); ?>
    <div class="container">
      <h1>Ajouter des produits</h1>
      <form method="post" action="fournisseur.php">

        <div class="form-group">
          <label for="nom">Nom du vendeur : </label>
          <input type="text" class="form-control" id="nom" name="nom" placeholder="Votre nom de vendeur">
        </div>

        <div class="form-group">
          <label for="nom_produit">Nom des produits : </label>
          <input type="text" class="form-control" id="nom_produit" name="nom_produit" placeholder="Le nom du produit">
        </div>

        <div class="form-group">
          <label for="volume">Volume unitaire : </label>
          <input type="text" class="form-control" id="volume" name="volume" placeholder="Entrez le volume unitaire du produit">
        </div>

        <div class="form-group">
          <label for="prix">Prix unitaire : </label>
          <input type="number" class="form-control" id="prix" name="prix" placeholder="Entrez le prix unitaire du produit">
        </div>

        <div class="form-group">
          <label for="quantite">Quantité totale : </label>
          <input type="number" class="form-control" id="quantite" name="quantite" placeholder="Entrez la quantité totale de produits">
        </div>

        <div class="form-group">
          <input type="hidden" name="id" value="<?php echo $_GET['id']?>" />
        </div>
  
        <button type="submit" class="btn btn-primary">Valider</button>
      </form>
    </div>

    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="jquery-3.1.1.slim.min.js"></script>
    <script src="tether.min.js"></script>
    <script src="bootstrap.min.js"></script>
  </body>
</html>
